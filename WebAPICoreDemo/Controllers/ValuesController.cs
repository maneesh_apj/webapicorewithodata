﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using WebAPICoreDemo.Models;
namespace WebAPICoreDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private List<Students> students = new List<Students>();
       
        public ValuesController()
        {
            students.Add(new Students { RollNumber = 1, Name = "Amit", ClassName = "UKG" });
            students.Add(new Students { RollNumber = 2, Name = "Sumit", ClassName = "four" });
            students.Add(new Students { RollNumber = 3, Name = "Arnav", ClassName = "LKG" });
            students.Add(new Students { RollNumber = 4, Name = "Gaurav", ClassName = "Two" });
            students.Add(new Students { RollNumber = 5, Name = "Kabya", ClassName = "Three" });
        }
        // GET api/values
        [HttpGet]
        [EnableQuery]
        public ActionResult<List<Students>> Get()
        {
            return students;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
