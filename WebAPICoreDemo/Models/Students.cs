﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPICoreDemo.Models
{
    public class Students
    {
        public string Name { get; set; }
        public string ClassName { get; set; }
        public int RollNumber { get; set; }

    }
}
